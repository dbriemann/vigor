package vigor

import (
	"github.com/tanema/gween"
	"github.com/tanema/gween/ease"
)

// object represents any entity that has a position, updates. It can either be with or without motion.
type object struct {
	tweenX         *gween.Tween
	tweenY         *gween.Tween
	pos            Vec2[float32]
	lastPos        Vec2[float32]
	vel            Vec2[float32]
	accel          Vec2[float32]
	dim            Vec2[uint32]
	id             uint64
	motionDisabled bool
}

func newObject() (o object) {
	G.idcounter++
	o.id = G.createId()
	o.motionDisabled = true
	return
}

func (o *object) Id() uint64 {
	return o.id
}

func (o *object) TweenTo(x, y, duration float32, f ease.TweenFunc) {
	o.SetMotion(true)
	o.tweenX = gween.New(o.pos[0], x, duration, f)
	o.tweenY = gween.New(o.pos[1], y, duration, f)
}

func (o *object) SetPos(x, y float32) {
	o.lastPos[0] = x
	o.lastPos[1] = y
	o.pos[0] = x
	o.pos[1] = y
}

func (o *object) Move(dx, dy float32) {
	o.lastPos[0] = o.pos[0]
	o.lastPos[1] = o.pos[1]
	o.pos[0] += dx
	o.pos[1] += dy
}

func (o *object) SetVel(x, y float32) {
	o.vel[0] = x
	o.vel[1] = y
	o.SetMotion(true)
}

func (o *object) SetAccel(x, y float32) {
	o.accel[0] = x
	o.accel[1] = y
	o.SetMotion(true)
}

func (o *object) SetDim(x, y uint32) {
	o.dim[0] = x
	o.dim[1] = y
}

func (o *object) SetMotion(enabled bool) {
	o.motionDisabled = !enabled
}

func (o *object) PixelPos() Vec2[int] {
	// TODO: is Round better than Floor for pixel perfect positions?
	return Vec2Floor[float32, int](o.pos)
}

func (o *object) Pos() *Vec2[float32] {
	return &o.pos
}

func (o *object) Vel() *Vec2[float32] {
	return &o.vel
}

func (o *object) Accel() *Vec2[float32] {
	return &o.accel
}

func (o *object) Dim() *Vec2[uint32] {
	return &o.dim
}

func (o *object) BBox() *Rect[int] {
	return &Rect[int]{
		Pos: Vec2Floor[float32, int](o.pos),
		Dim: Vec2ToType[uint32, int](o.dim),
	}
}

func (o *object) Update() {
	if o.motionDisabled {
		return
	}

	isTweening := false
	dt := G.Dt()
	if o.tweenX != nil {
		newx, finishedx := o.tweenX.Update(dt)
		o.pos[0] = newx
		if finishedx {
			o.tweenX = nil
		}
	}
	if o.tweenY != nil {
		newy, finishedy := o.tweenY.Update(dt)
		o.pos[1] = newy
		if finishedy {
			o.tweenY = nil
		}
	}

	// If tweening is active we do not do the normal update routine.
	if isTweening {
		return
	}

	// TODO: angular velocity

	o.pos[0] += o.vel[0] * G.Dt()
	o.pos[1] += o.vel[1] * G.Dt()

	o.vel[0] += o.accel[0] * G.Dt()
	o.vel[1] += o.accel[1] * G.Dt()
}

type Positionable interface {
	Pos() *Vec2[float32]
	Dim() *Vec2[uint32]
}

func Collides(obj1, obj2 Positionable) bool {
	// TODO: use lastPos for better collision detection.

	// If one of the rectangles does have a zero area, there is no intersection.
	if obj1.Dim()[0] <= 0 || obj2.Dim()[0] <= 0 || obj1.Dim()[1] <= 0 || obj2.Dim()[1] <= 0 {
		return false
	}

	// If one of the rectangles is left of the other, there is no intersection.
	if obj1.Pos()[0] > obj2.Pos()[0]+float32(obj2.Dim()[0]) || obj1.Pos()[0]+float32(obj1.Dim()[0]) < obj2.Pos()[0] {
		return false
	}

	// If one of the rectangles is above the other, there is no intersection.
	if obj1.Pos()[1] > obj2.Pos()[1]+float32(obj2.Dim()[1]) || obj1.Pos()[1]+float32(obj1.Dim()[1]) < obj2.Pos()[1] {
		return false
	}

	return true
}
