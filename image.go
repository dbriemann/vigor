package vigor

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/colorm"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

var _ effected = (*Image)(nil)

// Image is similar to a sprite, but can be altered and is not animated.
type Image struct {
	image   *ebiten.Image
	effects []Effect
	object
	visual
	visible bool
}

// CopyImage creates a new instance of a vigor.Image from another instance.
// This is a light weight copy and will refer to the same image in GPU memory.
// It will be drawn with the same draw call as all copies of the same image.
// The differences are the position, applied effects etc.
func CopyImage(img *Image) *Image {
	i := &Image{
		image:   img.image,
		visible: img.visible,
		visual:  img.visual,
		object:  img.object,
		effects: img.effects,
	}
	return i
}

func NewImage(name string) *Image {
	i := &Image{
		object:  newObject(),
		visual:  newVisual(),
		effects: []Effect{},

		visible: true,
		image:   G.assets.GetImageOrPanic(name),
	}

	i.SetDim(uint32(i.image.Bounds().Dx()), uint32(i.image.Bounds().Dy()))
	return i
}

func NewCanvas(width, height int) *Image {
	c := &Image{
		object:  newObject(),
		visual:  newVisual(),
		effects: []Effect{},

		visible: true,
		image:   ebiten.NewImage(width, height),
	}
	c.SetDim(uint32(width), uint32(height))

	return c
}

func (i *Image) InView(view Rect[int]) bool {
	return view.Intersects(*i.BBox())
}

func (i *Image) Clear() {
	i.image.Clear()
}

func (i *Image) ApplyEffect(e Effect) {
	e.Reset()
	e.Start()
	i.effects = append(i.effects, e)
}

func (i *Image) Update() {
	i.object.Update()
	for j := 0; j < len(i.effects); j++ {
		finished := i.effects[j].Update()
		if finished {
			i.effects = append(i.effects[:j], i.effects[j+1:]...)
		}
	}
}

func (c *Image) draw(target *ebiten.Image, op colorm.DrawImageOptions) {
	cm := colorm.ColorM{}
	c.transform(&op, int(c.Dim()[0]), int(c.Dim()[1]))
	op.GeoM.Translate(float64(c.PixelPos()[0]), float64(c.PixelPos()[1]))
	for i := 0; i < len(c.effects); i++ {
		c.effects[i].modifyDraw(&op)
	}
	colorm.DrawImage(target, c.image, cm, &op)
	for i := 0; i < len(c.effects); i++ {
		c.effects[i].draw(target, op)
	}
}

func (s *Image) Show(v bool) {
	s.visible = v
}

func (c *Image) Visible() bool {
	return c.visible
}

func (c *Image) DrawText(message string, t *Text, x, y float64) {
	op := text.DrawOptions{}
	op.ColorScale.ScaleWithColor(color.White)
	op.GeoM.Translate(x, y)
	op.LayoutOptions = t.layout
	text.Draw(c.image, message, &t.face, &op)
}

func (c *Image) DrawFilledRect(x, y, width, height float32, col color.Color, antialias bool) {
	vector.DrawFilledRect(c.image, x, y, width, height, col, antialias)
}

func (c *Image) DrawRect(x, y, width, height, strokeWidth float32, col color.Color, antialias bool) {
	vector.StrokeRect(c.image, x, y, width, height, strokeWidth, col, antialias)
}

func (c *Image) DrawCircle(x, y, radius, strokeWidth float32, col color.Color, antialias bool) {
	vector.StrokeCircle(c.image, x, y, radius, strokeWidth, col, antialias)
}

// TODO: other shapes
