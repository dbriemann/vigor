package vigor

import (
	"math"

	"golang.org/x/exp/constraints"
)

type Number interface {
	constraints.Integer | constraints.Float
}

type Vec2[T Number] [2]T

func (v Vec2[T]) Normalize() (out Vec2[T]) {
	if v[0] == 0 && v[1] == 0 {
		return v
	}
	len := math.Sqrt(float64(v[0]*v[0] + v[1] + v[1]))
	out[0] = T(float64(v[0]) / len)
	out[1] = T(float64(v[1]) / len)
	return
}

func (v Vec2[T]) Rotate(rad float64) (out Vec2[T]) {
	out[0] = T(math.Cos(rad))*v[0] - T(math.Sin(rad))*v[1]
	out[1] = T(math.Sin(rad))*v[0] - T(math.Cos(rad))*v[1]
	return
}

func (v Vec2[T]) Multiply(s T) (out Vec2[T]) {
	out[0] = v[0] * s
	out[1] = v[1] * s
	return
}

func Vec2ToType[T, U Number](in Vec2[T]) (out Vec2[U]) {
	out[0] = U(in[0])
	out[1] = U(in[1])
	return
}

func Vec2Floor[T Number, U constraints.Integer](in Vec2[T]) (out Vec2[U]) {
	out[0] = U(math.Floor(float64(in[0])))
	out[1] = U(math.Floor(float64(in[1])))
	return
}

type Rect[T Number] struct {
	Pos Vec2[T]
	Dim Vec2[T]
}

func (r Rect[T]) Intersects(other Rect[T]) bool {
	// If one of the rectangles does have a zero area, there is no intersection.
	if r.Dim[0] <= 0 || other.Dim[0] <= 0 || r.Dim[1] <= 0 || other.Dim[1] <= 0 {
		return false
	}

	// If one of the rectangles is left of the other, there is no intersection.
	if r.Pos[0] > other.Pos[0]+other.Dim[0] || r.Pos[0]+r.Dim[0] < other.Pos[0] {
		return false
	}

	// If one of the rectangles is above the other, there is no intersection.
	if r.Pos[1] > other.Pos[1]+other.Dim[1] || r.Pos[1]+r.Dim[1] < other.Pos[1] {
		return false
	}

	return true
}

// func Intersects[T Number](r, r2 Rect[T]) bool {
// 	// If one of the rectangles does have a zero area, there is no intersection.
// 	if r.Dim.W <= 0 || r2.Dim.W <= 0 || r.Dim.H <= 0 || r2.Dim.H <= 0 {
// 		return false
// 	}

// 	// If one of the rectangles is left of the other, there is no intersection.
// 	if r.Point[0] > r2.Point[0]+r2.Dim.W || r.Point[0]+r.Dim.W < r2.Point[0] {
// 		return false
// 	}

// 	// If one of the rectangles is above the other, there is no intersection.
// 	if r.Point[1] > r2.Point[1]+r2.Dim.H || r.Point[1]+r.Dim.H < r2.Point[1] {
// 		return false
// 	}

// 	return true
// }
