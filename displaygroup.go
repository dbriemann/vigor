package vigor

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/colorm"
)

var _ stageable = (*DisplayGroup)(nil)

type DisplayGroup struct {
	// TODO: use better data structure than slice for adding, removing and being ordered.
	staged []stageable
	object
	visible bool
	id      uint64
}

func NewDisplayGroup() *DisplayGroup {
	g := &DisplayGroup{}
	G.idcounter++
	g.id = G.createId()
	return g
}

func (d *DisplayGroup) Id() uint64 {
	return d.id
}

func (d *DisplayGroup) Add(s stageable) {
	d.staged = append(d.staged, s)
}

func (d *DisplayGroup) Remove(s stageable) {
	id := s.Id()
	for i := 0; i < len(d.staged); i++ {
		if d.staged[i].Id() == id {
			d.staged = append(d.staged[:i], d.staged[i+1:]...)
			return
		}
	}
}

func (d *DisplayGroup) InView(view Rect[int]) bool {
	return true
}

func (d *DisplayGroup) draw(target *ebiten.Image, op colorm.DrawImageOptions) {
	offset := G.internalGame.camera.view.Pos
	op.GeoM.Translate(-float64(offset[0]), -float64(offset[1]))
	for i := 0; i < len(d.staged); i++ {
		if d.staged[i].Visible() && G.internalGame.camera.inView(d.staged[i]) {
			d.staged[i].draw(target, op)
		}
	}
}

func (d *DisplayGroup) Update() {
	for i := 0; i < len(d.staged); i++ {
		d.staged[i].Update()
	}
}

func (d *DisplayGroup) Visible() bool {
	return d.visible
}

func (d *DisplayGroup) Show(v bool) {
	d.visible = v
}
