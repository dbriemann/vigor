package vigor

import (
	"github.com/hajimehoshi/ebiten/v2"
)

// camera represents a view of the world. It projects the contents of
// the viewport to a section of or the whole window.
type camera struct {
	canvas      *ebiten.Image
	followPoint *Vec2[float32]
	object
	bounds Rect[int]
	view   Rect[int]
	visual
}

// SetBounds defines the world space in which the camera can move.
// The view is confined within bounds.
func (c *camera) SetBounds(x, y int, width, height uint) {
	c.bounds.Pos[0] = x
	c.bounds.Pos[1] = y
	c.bounds.Dim[0] = int(width)
	c.bounds.Dim[1] = int(height)
}

func (c *camera) SetView(x, y int, width, height uint) {
	c.SetViewPos(x, y)
	c.ResizeView(width, height)
}

// SetViewPos sets the position of the viewport of the camera.
func (c *camera) SetViewPos(x, y int) {
	if c.view.Pos[0] == x &&
		c.view.Pos[1] == y {
		return
	}

	c.view.Pos[0] = x
	if c.view.Pos[0] < c.bounds.Pos[0] {
		c.view.Pos[0] = c.bounds.Pos[0]
	} else if c.view.Pos[0]+c.view.Dim[0] > c.bounds.Pos[0]+c.bounds.Dim[0] {
		c.view.Pos[0] = c.bounds.Pos[0] + c.bounds.Dim[0] - c.view.Dim[0]
	}
	c.view.Pos[1] = y
	if c.view.Pos[1] < c.bounds.Pos[1] {
		c.view.Pos[1] = c.bounds.Pos[1]
	} else if c.view.Pos[1]+c.view.Dim[1] > c.bounds.Pos[1]+c.bounds.Dim[1] {
		c.view.Pos[1] = c.bounds.Pos[1] + c.bounds.Dim[1] - c.view.Dim[0]
	}

	G.Debug("camera.SetView", "pos", c.view.Pos)
}

func (c *camera) ResizeView(width, height uint) {
	if width == 0 {
		width = uint(c.view.Dim[0])
	}
	if height == 0 {
		height = uint(c.view.Dim[1])
	}

	if c.view.Dim[0] == int(width) &&
		c.view.Dim[1] == int(height) {
		// Size did not change.
		return
	}

	c.view.Dim[0] = int(width)
	c.view.Dim[1] = int(height)

	G.Debug("camera.ResizeView", "dim", c.view.Dim)
	c.canvas = ebiten.NewImage(int(width), int(height))
}

func (c *camera) draw(target *ebiten.Image) {
	op := ebiten.DrawImageOptions{}
	// TODO: should scaling keep ratio? modes?
	xscale := float64(G.window[0]) / float64(c.view.Dim[0])
	yscale := float64(G.window[1]) / float64(c.view.Dim[1])
	op.GeoM.Scale(xscale, yscale)
	target.DrawImage(c.canvas, &op)
}

func (c *camera) inView(s stageable) bool {
	return s.InView(c.view)
}

func (c *camera) Follow(centerPoint *Vec2[float32]) {
	c.followPoint = centerPoint
}

func (c *camera) SetCenter(x, y int) {
	G.Debug("camera: set center", "x", x, "y", y)
	nx := x - c.view.Dim[0]/2
	ny := y - c.view.Dim[1]/2
	c.SetViewPos(nx, ny)
}

func (c *camera) Update() {
	if c.followPoint != nil {
		c.SetCenter(int(c.followPoint[0]), int(c.followPoint[1]))
	}
}
