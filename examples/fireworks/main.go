package main

import (
	"fmt"
	"math"
	"math/rand"

	"github.com/dbriemann/vigor"
	"github.com/tanema/gween/ease"
)

const (
	screenWidth  = 800
	screenHeight = 1200
	windowWidth  = screenWidth
	windowHeight = screenHeight
	worldWidth   = 5 * screenWidth
	worldHeight  = 5 * screenHeight

	rocketAmount          = 20
	explosionParticlesMin = 1000
	explosionParticlesMax = 3000
	particleSpeedMax      = 200
	propulsionParticles   = 300
)

var gravity = vigor.Vec2[float32]{0, 100}

type Game struct {
	hudCanvas *vigor.Image
	text      *vigor.Text
	rockets   []*Rocket
}

func (g *Game) Init() {
	particles := []*vigor.Image{
		vigor.NewImage("particle_cyan"),
		vigor.NewImage("particle_green"),
		vigor.NewImage("particle_orange"),
		vigor.NewImage("particle_pink"),
		vigor.NewImage("particle_red"),
		vigor.NewImage("particle_white"),
		vigor.NewImage("particle_yellow"),
	}
	g.rockets = make([]*Rocket, rocketAmount)
	for i := range rocketAmount {
		g.rockets[i] = NewRocket(particles)
		vigor.G.Add(g.rockets[i].propulsion)
		vigor.G.Add(g.rockets[i].explosion)
		g.rockets[i].Start()
	}

	g.text = vigor.NewText("source_serif", 18)
	g.hudCanvas = vigor.NewCanvas(windowWidth, windowHeight)
	vigor.G.AddToHUD(g.hudCanvas)
}

func (g *Game) Update() {
	g.hudCanvas.Clear()
	fpsStr := fmt.Sprintf("FPS: %f", vigor.G.FPS())
	g.hudCanvas.DrawText(fpsStr, g.text, 5, 5)
	for i := range rocketAmount {
		g.rockets[i].Update()
	}
}

func (g *Game) OnWindowSizeChanged(w, h int) {
}

type Rocket struct {
	propulsion  *vigor.Emitter
	explosion   *vigor.Emitter
	blastHeight int
	exploded    bool
}

func NewRocket(particles []*vigor.Image) *Rocket {
	colorsAmount := rand.Intn(2) + 1
	coloredParticles := make([]vigor.Image, colorsAmount)
	for i := range colorsAmount {
		r := rand.Intn(len(particles))
		coloredParticles[i] = *vigor.CopyImage(particles[r])
	}

	r := &Rocket{}

	propulsionOpts := vigor.EmitterOpts{
		Capacity:       propulsionParticles,
		Rate:           0,
		AngleMinMax:    vigor.Vec2[float32]{math.Pi * 0.4, math.Pi * 0.6},
		LifetimeMinMax: vigor.Vec2[float32]{0.2, 0.4},
		SpeedMinMax:    vigor.Vec2[float32]{150, 200},
		Acceleration:   gravity,
	}
	r.propulsion = vigor.NewParticleEmitter([]vigor.Image{*vigor.NewImage("particle_white")}, propulsionOpts)

	amount := explosionParticlesMin + rand.Intn(explosionParticlesMax-explosionParticlesMin)
	maxSpeed := 100 + rand.Intn(particleSpeedMax-100)
	explosionOpts := vigor.EmitterOpts{
		Capacity:       amount,
		Rate:           0,
		AngleMinMax:    vigor.Vec2[float32]{0, 2 * math.Pi},
		LifetimeMinMax: vigor.Vec2[float32]{1, 1.5},
		SpeedMinMax:    vigor.Vec2[float32]{2, float32(maxSpeed)},
		Acceleration:   gravity,
	}
	r.explosion = vigor.NewParticleEmitter(coloredParticles, explosionOpts)
	r.explosion.SetTTL(1.0, 1.5)
	return r
}

func (r *Rocket) Update() {
	pos := r.propulsion.PixelPos()
	if r.blastHeight <= pos[1] {
		r.propulsion.Update()
	} else {
		r.propulsion.SetRate(0)
		if !r.exploded {
			pos := r.propulsion.Pos()
			r.explosion.SetPos(pos[0], pos[1])
			r.explosion.Show(true)
			r.explosion.Burst()
			r.exploded = true
			r.explosion.SetRate(0)
		}
		r.explosion.Update()

		if r.explosion.ActiveParticles() == 0 {
			r.explosion.Show(false)
			r.Start()
		}
	}
}

func (r *Rocket) Start() {
	maxSpeed := 100 + rand.Intn(particleSpeedMax-100)
	r.explosion.SetSpeed(2, float32(maxSpeed))
	x := rand.Intn(screenWidth)
	y := screenHeight
	r.exploded = false
	r.blastHeight = screenHeight - (rand.Intn(screenHeight/5) + screenHeight*3/5)
	r.propulsion.Show(true)
	r.propulsion.SetRate(propulsionParticles)
	r.propulsion.SetPos(float32(x), float32(y))
	ttl := 2 + rand.Float32()
	r.propulsion.TweenTo(float32(x), float32(screenHeight)*0.1, ttl, ease.InCirc)
}

func main() {
	g := Game{}
	opts := vigor.GameOptions{
		WindowTitle:  "Fireworks",
		WindowWidth:  windowWidth,
		WindowHeight: windowHeight,
	}

	vigor.InitGame(&g, opts)

	vigor.G.Camera().SetView(0, 0, screenWidth, screenHeight)
	vigor.G.Camera().SetCenter(screenWidth/2, screenHeight/2)

	vigor.RunGame()
}
