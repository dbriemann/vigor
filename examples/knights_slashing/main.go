package main

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"time"

	"github.com/dbriemann/vigor"
	input "github.com/quasilyte/ebitengine-input"
	"github.com/tanema/gween/ease"
)

const (
	screenWidth    = 320
	screenHeight   = 240
	windowWidth    = 4 * screenWidth
	windowHeight   = 4 * screenHeight
	frameWidth     = 120
	frameHeight    = 80
	bgKnightsCount = 10

	ActionSlower input.Action = iota
	ActionFaster
	ActionPrevEaseFunc
	ActionNextEaseFunc
)

func GetFunctionName(i interface{}) string {
	strs := strings.Split((runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()), ".")
	return strs[len(strs)-1]
}

var (
	easeFuncs = []ease.TweenFunc{
		ease.Linear,
		ease.InQuad, ease.OutQuad, ease.InOutQuad, ease.OutInQuad,
		ease.InCubic, ease.OutCubic, ease.InOutCubic, ease.OutInCubic,
		ease.InQuart, ease.OutQuart, ease.InOutQuart, ease.OutInQuart,
		ease.InQuint, ease.OutQuint, ease.InOutQuint, ease.OutInQuint,
		ease.InExpo, ease.OutExpo, ease.InOutExpo, ease.OutInExpo,
		ease.InSine, ease.OutSine, ease.InOutSine, ease.OutInSine,
		ease.InCirc, ease.OutCirc, ease.InOutCirc, ease.OutInCirc,
		ease.InBack, ease.OutBack, ease.InOutBack, ease.OutInBack,
		ease.InBounce, ease.OutBounce, ease.InOutBounce, ease.OutInBounce,
		ease.InElastic, ease.OutElastic, ease.InOutElastic, ease.OutInElastic,
	}

	keymap = input.Keymap{
		ActionSlower:       {input.KeyGamepadDown, input.KeyDown},
		ActionFaster:       {input.KeyGamepadUp, input.KeyUp},
		ActionPrevEaseFunc: {input.KeyGamepadLeft, input.KeyLeft},
		ActionNextEaseFunc: {input.KeyGamepadRight, input.KeyRight},
	}
)

type Game struct {
	bgKnights  []*Knight
	knight     *Knight
	input      *input.Handler
	text       *vigor.Text
	textCanvas *vigor.Image
	dur        time.Duration
	funcIndex  int
}

func (g *Game) Init() {
	g.input = vigor.NewInputHandler(0, keymap)

	g.knight = NewKnight(0, 0)
	g.knight.SetPos(screenWidth/2-float32(frameWidth/2), screenHeight/2-float32(frameHeight/2))
	vigor.G.Add(g.knight)

	g.bgKnights = make([]*Knight, bgKnightsCount)
	for i := range bgKnightsCount {
		g.bgKnights[i] = NewKnight(0, 0)
		g.bgKnights[i].SetDuration(time.Millisecond * time.Duration(500+i*100))
		g.bgKnights[i].SetPos(float32(screenWidth*i)/bgKnightsCount, float32(screenHeight/2+frameHeight))
		g.bgKnights[i].Scale(0.3, 0.3)
		vigor.G.Add(g.bgKnights[i])
	}

	g.text = vigor.NewText("raider_knight", 32)

	g.textCanvas = vigor.NewCanvas(windowWidth, windowHeight)
	vigor.G.AddToHUD(g.textCanvas)
}

func (g *Game) Update() {
	g.textCanvas.Clear()
	if g.input.ActionIsJustPressed(ActionFaster) {
		g.dur += 100 * time.Millisecond
		g.knight.SetDuration(g.dur)
	} else if g.input.ActionIsJustPressed(ActionSlower) {
		if g.dur >= 200*time.Millisecond {
			g.dur -= 100 * time.Millisecond
			g.knight.SetDuration(g.dur)
		}
	} else if g.input.ActionIsJustPressed(ActionPrevEaseFunc) {
		if g.funcIndex > 0 {
			g.funcIndex--
			g.knight.SetTweenFunc(easeFuncs[g.funcIndex])
		}
	} else if g.input.ActionIsJustPressed(ActionNextEaseFunc) {
		if g.funcIndex < len(easeFuncs)-1 {
			g.funcIndex++
			g.knight.SetTweenFunc(easeFuncs[g.funcIndex])
		}
	}

	msg := fmt.Sprintf("Ease func: %s (left/right arrows)\nDuration: %s (up/down arrows)",
		GetFunctionName(easeFuncs[g.funcIndex]),
		g.dur,
	)
	g.textCanvas.DrawText(msg, g.text, 10, 10)
}

func (g *Game) OnWindowSizeChanged(w, h int) {
}

type Knight struct {
	vigor.Sprite
}

func (k *Knight) Update() {
	k.Sprite.Update()
}

func NewKnight(x, y int) *Knight {
	k := &Knight{
		Sprite: *vigor.NewSprite("knight_attack1"),
	}
	k.SetPos(float32(x), float32(y))
	return k
}

func main() {
	g := Game{
		dur:       700 * time.Millisecond,
		funcIndex: 0,
	}
	opts := vigor.GameOptions{
		WindowTitle:  "Knight Animation Demo",
		WindowWidth:  windowWidth,
		WindowHeight: windowHeight,
	}

	vigor.InitGame(&g, opts)

	vigor.G.Camera().SetView(0, 0, screenWidth, screenHeight)
	vigor.G.Camera().SetCenter(screenWidth/2, screenHeight/2)

	vigor.RunGame()
}
