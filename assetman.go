package vigor

import (
	"fmt"
	"image"
	"os"
	"path"
	"time"

	_ "image/jpeg"
	_ "image/png"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type assetManager struct {
	// TODO: audio
	// TODO: others
	Fonts              map[string]*text.GoTextFaceSource
	Images             map[string]*ebiten.Image
	Sections           map[string]Section
	AnimationTemplates map[string]*AnimationTemplate
	RootPath           string
}

func newAssetManager() assetManager {
	G.Info("creating asset manager..")
	r := assetManager{
		Fonts:              map[string]*text.GoTextFaceSource{},
		Images:             map[string]*ebiten.Image{},
		Sections:           map[string]Section{},
		AnimationTemplates: map[string]*AnimationTemplate{},
	}
	return r
}

func (r *assetManager) LoadConfig(fname string) error {
	cfg, err := loadConfigData[ResourceConfig](fname)
	if err != nil {
		return err
	}

	r.RootPath = cfg.ResourceRoot

	G.Info("loading images..")
	for relPath, name := range cfg.Images {
		absPath := path.Join(r.RootPath, relPath)
		f, err := os.Open(absPath)
		if err != nil {
			return err
		}
		defer f.Close()

		img, _, err := image.Decode(f)
		if err != nil {
			return err
		}
		ebImg := ebiten.NewImageFromImage(img)

		r.Images[name] = ebImg
		G.Info("loaded image", "name", name, "path", absPath)
	}

	// TODO: audio
	G.Info("loading audio.. (TODO)")

	G.Info("loading fonts..")
	for relPath, font := range cfg.Fonts {
		absPath := path.Join(r.RootPath, relPath)
		f, err := os.Open(absPath)
		if err != nil {
			return err
		}
		defer f.Close()

		source, err := text.NewGoTextFaceSource(f)
		if err != nil {
			return err
		}
		r.Fonts[font] = source
		G.Info("loaded font", "name", font, "path", absPath)
	}

	for name, sec := range cfg.Sections {
		r.Sections[name] = NewSection(sec.Left, sec.Top, sec.Width, sec.Height, sec.Padding)
	}

	G.Info("loading animations..")
	for animName, template := range cfg.Animations {
		imgName := template.ImageName
		img, ok := r.Images[imgName]
		if !ok {
			return fmt.Errorf("%w: %s", ErrImageNotLoaded, imgName)
		}

		if template.EaseFunc == "" {
			template.EaseFunc = "Linear"
		}
		f, ok := easeFuncMappings[template.EaseFunc]
		if !ok {
			return fmt.Errorf("%w: %s", ErrUnknownEaseFunc, template.EaseFunc)
		}
		a, err := NewAnimationTemplate(
			img,
			r.Sections[template.SectionName],
			template.Width,
			template.Height,
			template.Frames,
			time.Duration(template.Duration*float64(time.Second)),
			template.Looped,
			f,
		)
		if err != nil {
			return err
		}
		r.AnimationTemplates[animName] = a
		G.Info("loaded animation", "name", animName, "image", imgName)
	}

	return nil
}

func (r *assetManager) GetImageOrPanic(name string) *ebiten.Image {
	img, ok := r.Images[name]
	if !ok {
		panic(fmt.Sprintf("could not load image %s from asset manager: does not exist", name))
	}
	return img
}

func (r *assetManager) GetFontOrPanic(name string) *text.GoTextFaceSource {
	font, ok := r.Fonts[name]
	if !ok {
		panic(fmt.Sprintf("could not load font %s from asset manager: does not exist", name))
	}
	return font
}

func (r *assetManager) GetAnimTemplateOrPanic(name string) *AnimationTemplate {
	templ, ok := r.AnimationTemplates[name]
	if !ok {
		panic(fmt.Sprintf("could not load animation template %s from asset manager: does not exist", name))
	}
	return templ
}
