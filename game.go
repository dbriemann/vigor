package vigor

import (
	"io"
	"log/slog"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/colorm"
	ebinput "github.com/quasilyte/ebitengine-input"
)

type Game interface {
	Init()
	Update()
	OnWindowSizeChanged(width, height int)
}

type internalGame struct {
	effects []Effect
	stage   *DisplayGroup
	hud     *DisplayGroup
	camera  camera
	input   ebinput.System
}

func (g *internalGame) Draw(target *ebiten.Image) {
	G.internalGame.camera.canvas.Clear()
	op := colorm.DrawImageOptions{}
	for i := 0; i < len(g.effects); i++ {
		g.effects[i].modifyDraw(&op)
	}
	// TODO: drawing to camera canvas needs to translate all objects
	g.stage.draw(G.internalGame.camera.canvas, op)
	for i := 0; i < len(g.effects); i++ {
		g.effects[i].draw(G.internalGame.camera.canvas, op)
	}
	G.internalGame.camera.draw(target)
	g.hud.draw(target, op)
}

func (g *internalGame) Update() error {
	g.input.Update()
	g.camera.Update()
	g.stage.Update()
	g.hud.Update()
	for j := 0; j < len(g.effects); j++ {
		finished := g.effects[j].Update()
		if finished {
			g.effects = append(g.effects[:j], g.effects[j+1:]...)
		}
	}
	G.externalGame.Update()
	return nil
}

func (g *internalGame) add(s stageable) {
	g.stage.Add(s)
}

func (g *internalGame) addToHUD(s stageable) {
	g.hud.Add(s)
}

func (g *internalGame) Layout(width, height int) (int, int) {
	G.externalGame.OnWindowSizeChanged(width, height)

	// TODO: what to do with g.window?
	if width != G.window[0] || height != G.window[1] {
		SetWindowSize(width, height)
	}
	return width, height
}

type GameOptions struct {
	WindowTitle        string
	WindowWidth        int
	WindowHeight       int
	WindowResizingMode WindowResizingModeType

	// Optional
	LogTarget  io.Writer
	LogOptions *slog.HandlerOptions
}

func InitGame(g Game, opts GameOptions) {
	if opts.LogTarget == nil {
		// TODO: should write to file by default?
		opts.LogTarget = os.Stderr
	}
	slogOpts := &slog.HandlerOptions{
		// TODO: make level configurable
		Level: slog.LevelDebug,
	}
	G.Logger = slog.New(slog.NewTextHandler(opts.LogTarget, slogOpts))
	G.Info("init game")

	ebiten.SetWindowTitle(opts.WindowTitle)
	SetWindowSize(opts.WindowWidth, opts.WindowHeight)
	SetWindowResizingMode(opts.WindowResizingMode)

	G.assets = newAssetManager()

	if err := G.assets.LoadConfig(configFilePath); err != nil {
		panic(err)
	}

	G.SetTPS(60)

	G.internalGame.stage = NewDisplayGroup()
	G.internalGame.hud = NewDisplayGroup()

	G.internalGame.input.Init(ebinput.SystemConfig{
		DevicesEnabled: ebinput.AnyDevice,
	})

	G.externalGame = g
	G.externalGame.Init()

	// By default the camera covers the logical screen size.
	G.internalGame.camera.SetBounds(0, 0, uint(opts.WindowWidth), uint(opts.WindowHeight))
	G.internalGame.camera.SetViewPos(0, 0)
	G.internalGame.camera.ResizeView(uint(opts.WindowWidth), uint(opts.WindowHeight))
}

func RunGame() error {
	return ebiten.RunGame(&G.internalGame)
}
