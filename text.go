package vigor

import (
	"github.com/hajimehoshi/ebiten/v2/text/v2"
)

type Text struct {
	face              text.GoTextFace
	layout            text.LayoutOptions
	linespacingFactor float64
}

type Align = text.Align

const (
	AlignStart  = text.AlignStart
	AlignCenter = text.AlignCenter
	AlignEnd    = text.AlignEnd
)

func NewText(fontFaceName string, size float64) *Text {
	font := G.assets.GetFontOrPanic(fontFaceName)

	t := &Text{
		face: text.GoTextFace{
			Source: font,
			Size:   size,
		},
	}

	t.SetLayout(1.2, AlignStart, AlignStart)

	return t
}

func (t *Text) SetFontSize(size float64) {
	t.face.Size = size
	t.layout.LineSpacing = t.linespacingFactor * t.face.Size
}

func (t *Text) SetLayout(lineSpacing float64, primaryAlign, secondoryAlign Align) {
	t.layout.PrimaryAlign = primaryAlign
	t.layout.SecondaryAlign = secondoryAlign
	t.linespacingFactor = lineSpacing
	t.layout.LineSpacing = t.linespacingFactor * t.face.Size
}
