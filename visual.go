package vigor

import (
	"github.com/hajimehoshi/ebiten/v2/colorm"
)

type visual struct {
	scale   Vec2[float32]
	visible bool
}

func newVisual() visual {
	v := visual{
		scale:   Vec2[float32]{1, 1},
		visible: true,
	}
	return v
}

func (v *visual) FlipX() {
	v.scale[0] *= -1
}

func (v *visual) FlipY() {
	v.scale[1] *= -1
}

func (v *visual) Scale(x, y float32) {
	v.scale[0] *= x
	v.scale[1] *= y
}

func (v *visual) Visible() bool {
	return v.visible
}

func (v *visual) Show(on bool) {
	v.visible = on
}

func (v *visual) transform(op *colorm.DrawImageOptions, width, height int) {
	tx := 0.0
	ty := 0.0

	op.GeoM.Scale(float64(v.scale[0]), float64(v.scale[1]))

	if v.scale[0] < 0 {
		tx = float64(width) * -1.0 * float64(v.scale[0])
	}
	if v.scale[1] < 0 {
		ty = float64(height) * -1.0 * float64(v.scale[1])
	}

	op.GeoM.Translate(tx, ty)

	// TODO: debug wireframe
	// vector.StrokeRect(target, s.pos[0], s.pos[1], float32(s.dim[0]), float32(s.dim[1]), 2, color.White, false)
}
