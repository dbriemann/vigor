package vigor

import (
	"math/rand"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/colorm"
)

var _ stageable = (*Emitter)(nil)

type particle struct {
	*Image

	ttl float32
}

// TODO: should ALL particles of ALL emitters be in one global array?

type Emitter struct {
	ptypes    []Image
	particles []*particle
	object
	size     int
	capacity int
	rate     int
	angle    Vec2[float32]
	lifetime Vec2[float32]
	speed    Vec2[float32]
	accel    Vec2[float32]
	toSpawn  float32
	active   bool
}

type EmitterOpts struct {
	Capacity       int
	Rate           int
	AngleMinMax    Vec2[float32]
	LifetimeMinMax Vec2[float32]
	SpeedMinMax    Vec2[float32]
	Acceleration   Vec2[float32]
}

func NewParticleEmitter(images []Image, opts EmitterOpts) *Emitter {
	e := &Emitter{
		object:   newObject(),
		capacity: opts.Capacity,
		size:     0,
		rate:     opts.Rate,
		angle:    opts.AngleMinMax,
		lifetime: opts.LifetimeMinMax,
		speed:    opts.SpeedMinMax,
		accel:    opts.Acceleration,

		ptypes:    images,
		particles: make([]*particle, opts.Capacity),
		active:    false,
		toSpawn:   0,
	}

	for i := 0; i < e.capacity; i++ {
		ptype := rand.Intn(len(e.ptypes))
		e.particles[i] = &particle{
			Image: CopyImage(&e.ptypes[ptype]),
			ttl:   0,
		}
	}

	return e
}

func (e *Emitter) InView(view Rect[int]) bool {
	// TODO: should single particles be view-checked?
	return true
}

func (e *Emitter) Burst() {
	for spawned := e.spawn(); spawned; spawned = e.spawn() {
	}
}

func (e *Emitter) SetTTL(min, max float32) {
	e.lifetime = Vec2[float32]{min, max}
}

func (e *Emitter) SetRate(r int) {
	e.rate = r
}

func (e *Emitter) SetAngle(min, max float32) {
	e.angle = Vec2[float32]{min, max}
}

func (e *Emitter) SetSpeed(min, max float32) {
	e.speed = Vec2[float32]{min, max}
}

func (e *Emitter) Id() uint64 {
	return e.id
}

func (e *Emitter) Visible() bool {
	return e.active
}

func (e *Emitter) Show(v bool) {
	e.active = v
}

func (e *Emitter) Update() {
	if !e.active {
		return
	}
	e.object.Update()

	// Spawn new particles.
	e.toSpawn += float32(e.rate) * G.Dt()
	amount := int(e.toSpawn)
	for i := 0; i < amount; i++ {
		if !e.spawn() {
			break
		}
		e.toSpawn -= 1
	}

	// Remove dead particles by swapping with last "good" one.
	for i := 0; i < e.size; i++ {
		p := e.particles[i]
		p.ttl -= G.Dt()
		if p.ttl <= 0 {
			e.size--
			swp := e.particles[e.size]
			e.particles[e.size] = e.particles[i]
			e.particles[i] = swp
			i--
		} else {
			// This one is still active and receives an update.
			e.particles[i].Update()
		}
	}
}

func (e *Emitter) ActiveParticles() int {
	return e.size
}

func (e *Emitter) draw(target *ebiten.Image, op colorm.DrawImageOptions) {
	for i := 0; i < e.size; i++ {
		e.particles[i].draw(target, op)
	}
}

func (e *Emitter) spawn() bool {
	if e.size >= e.capacity {
		return false
	}

	p := e.particles[e.size]
	e.size++
	p.ttl = 0
	p.SetPos(e.pos[0], e.pos[1])

	// Find random speed within range.
	speed := e.speed[0] + rand.Float32()*(e.speed[1]-e.speed[0])

	// Find random angle to emit. No rotation means right (1,0).
	rad := e.angle[0] + rand.Float32()*(e.angle[1]-e.angle[0])

	// Rotate default direction with random angle and apply speed.
	dir := Vec2[float32]{1, 0}
	rot := dir.Rotate(float64(rad))
	vec := rot.Multiply(speed)

	// And set the according velocity, acceleration ..
	p.SetVel(vec[0], vec[1])
	p.SetAccel(e.accel[0], e.accel[1])
	// TODO: rotation

	// Set random lifetime.
	lifetime := e.lifetime[0] + rand.Float32()*(e.lifetime[1]-e.lifetime[0])
	p.ttl = lifetime

	return true
}
