package vigor

import (
	"log/slog"

	"github.com/hajimehoshi/ebiten/v2"
	ebinput "github.com/quasilyte/ebitengine-input"
)

// TODO: make glob thread safe.
var G glob

type glob struct {
	*slog.Logger
	externalGame Game
	assets       assetManager
	internalGame internalGame
	idcounter    uint64
	tps          uint32
	dt           float32
	window       Vec2[int]
}

func (g *glob) createId() uint64 {
	g.idcounter++
	return g.idcounter
}

func (g *glob) Dt() float32 {
	return g.dt
}

func (g *glob) TPS() uint32 {
	return g.tps
}

func (g *glob) SetTPS(tps uint32) {
	if tps >= 1 {
		g.tps = tps
		g.dt = 1.0 / float32(tps)
	}
}

func (g *glob) FPS() float64 {
	return ebiten.ActualFPS()
}

func (g *glob) Add(s stageable) {
	g.internalGame.add(s)
}

func (g *glob) AddToHUD(s stageable) {
	g.internalGame.addToHUD(s)
}

func (g *glob) ApplyEffect(e Effect) {
	e.Reset()
	e.Start()
	g.internalGame.effects = append(g.internalGame.effects, e)
}

func (g *glob) Camera() *camera {
	return &g.internalGame.camera
}

func SetConfigFile(cfgFilePath string) {
	configFilePath = cfgFilePath
}

func SetWindowSize(w, h int) {
	G.Info("setting window size", "width", w, "height", h)
	G.window[0] = w
	G.window[1] = h
	ebiten.SetWindowSize(w, h)
}

type WindowResizingModeType int

var (
	WindowResizingDisabled       WindowResizingModeType = WindowResizingModeType(ebiten.WindowResizingModeDisabled)
	WindowResizingOnlyFullscreen WindowResizingModeType = WindowResizingModeType(ebiten.WindowResizingModeOnlyFullscreenEnabled)
	WindowResizingModeEnabled    WindowResizingModeType = WindowResizingModeType(ebiten.WindowResizingModeEnabled)
)

func SetWindowResizingMode(mode WindowResizingModeType) {
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeType(mode))
}

func NewInputHandler(id uint8, keymap ebinput.Keymap) *ebinput.Handler {
	return G.internalGame.input.NewHandler(id, keymap)
}
